class Proteina:
	def __init__(self, secu) -> None:
		if isinstance(secu, str):
			self.__nombre = None
			self.__descripcion = None
			self.__secuencia = secu
		else:
			print("Por favor ingrese una secuencia de aminoácidos válida")

	def set_nombre(self, nombre):
		if isinstance(nombre, str):
			self.__nombre = nombre

	def get_nombre(self):
		return self.__nombre
	
	def set_descrip(self, descrip):
		if isinstance(descrip, str):
			self.__descripcion = descrip

	def get_descrip(self):
		return self.__descripcion
	
	def get_secuencia(self):
		return self.__secuencia
	
