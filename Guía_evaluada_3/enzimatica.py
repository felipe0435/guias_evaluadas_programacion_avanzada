from proteina import Proteina

class Enzimatica(Proteina):
	def __init__(self, secu) -> None:
		super().__init__(secu)
		self.__substrato = None

	def set_substrato(self, sub):
		if isinstance (sub, str):
			self.__substrato = sub

	def get_substrato(self):
		return self.__substrato