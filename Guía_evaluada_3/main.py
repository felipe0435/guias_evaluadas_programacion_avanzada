from enzimatica import Enzimatica
from estructural import Estructural
from secuencia import Secuencia
from protemutante import Protemutante
from analizador import Analizador
from random import randint

def crea_secuencia():
	ami = ["ASP", "GLU", "ARG", "LYS", "ASN", "HIS", "GLN", "SER", "THR", 
	"ALA", "GLY", "VAL", "PRO", "LEU", "PHE", "TYR", "ILE", "MET", "TRP", "CYS"]
	secu = ""
	cant = randint(5, 100)
	for i in range(0, cant):
		num = randint(0, 19)
		secu = f"{secu}{ami[num]}"
	return secu

def main():
	secu1 = crea_secuencia()
	secu2 = crea_secuencia()
	secu3 = crea_secuencia()

	prote1 = Estructural(secu1)
	prote1.set_nombre("7KJ2")
	prote1.set_descrip("SARS-CoV-2 Spike Glycoprotein with one ACE2 Bound")
	prote1.set_tipo("globular")

	prote2 = Enzimatica(secu2)
	prote2.set_nombre("11BA")
	prote2.set_descrip("BINDING OF A SUBSTRATE ANALOGUE TO A DOMAIN SWAPPING PROTEIN IN THE COMPLEX OF BOVINE SEMINAL RIBONUCLEASE WITH URIDYLYL-2',5'-ADENOSINE")
	prote2.set_substrato("almidón")

	prote3 = Protemutante(secu3)
	prote3.set_nombre("1B21")
	prote3.set_descrip("DELETION OF A BURIED SALT BRIDGE IN BARNASE")
	prote3.set_mutacion("si")

	secuencia = Secuencia()
	secuencia.set_proteinas(prote1)
	secuencia.set_proteinas(prote2)

	analizador = Analizador()
	analizador.set_proteina(prote1)
	analizador.set_proteina(prote2)
	analizador.set_proteina(prote3)

	print(prote1.get_nombre(),"--", prote1.get_descrip(), "--", prote1.get_secuencia(), "--", prote1.get_tipo())
	print(prote2.get_nombre(), "--", prote2.get_descrip(), "--", prote2.get_secuencia(), "--", prote2.get_substrato())
	print(prote3.get_nombre(), "--", prote3.get_descrip(), "--", prote3.get_secuencia(), "--", prote3.get_mutacion())

	secuencia.mostrar_secuencia()

	analizador.porcentaje()
	analizador.peso_molecular()


if __name__ == "__main__":
	main()