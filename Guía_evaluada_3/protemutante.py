from proteina import Proteina

class Protemutante(Proteina):
	def __init__(self, secu) -> None:
		super().__init__(secu)
		self.__mutacion = None

	def set_mutacion(self, mut):
		if isinstance(mut, str):
			mut.lower()
			if mut == "si" or mut == "no":
				self.__mutacion = mut
		pass
	
	def get_mutacion(self):
		return self.__mutacion

	def mostrar_mutacion(self):
		print(f"mutación: {self.__mutacion}")