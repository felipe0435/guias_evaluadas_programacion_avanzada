from proteina import Proteina

class Analizador:
	def __init__(self) -> None:
		self.__proteinas = []

	def set_proteina(self, prote):
		if isinstance(prote, Proteina):
			self.__proteinas.append(prote)

	def porcentaje(self):
		for elem in self.__proteinas:
			ami = []
			cant = 0
			hidrofobicos = 0
			for carac in elem.get_secuencia()[::1]:
				ami.append(carac)
			for i in range (0, len(ami)):
				temp = ""
				if i == 0:
					cant += 1
					for c in range(0, 3):
						temp = temp + ami[c]
				elif i % 3 == 0:
					cant += 1
					for c in range(0, 3):
						temp = temp + ami[i + c]

				if (temp == "ALA" or temp == "Val" or temp == "ILE" or 
					temp == "LEU" or temp == "PHE" or temp == "MET"):
					hidrofobicos += 1

			porcentaje = (hidrofobicos / cant) * 100
			print (f"{elem.get_nombre()} tiene un {porcentaje}% de aminoácidos hidrofóbicos.")

	def peso_molecular(self):
		
		pass

				