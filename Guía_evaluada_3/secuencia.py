from proteina import Proteina

class Secuencia:
	def __init__(self) -> None:
		self.__proteinas = []

	def set_proteinas(self, prote):
		if isinstance(prote, Proteina):
			self.__proteinas.append(prote)

	def get_proteinas(self):
		return self.__proteinas

	def mostrar_secuencia(self):
		for elem in self.__proteinas:
			print(f"Secuencia de {elem.get_nombre()}: {elem.get_secuencia()}")
		