from proteina import Proteina

class Estructural(Proteina):
	def __init__(self, secu) -> None:
		super().__init__(secu)
		self.__tipo = None

	def set_tipo(self, tipo):
		if isinstance(tipo, str):
			tipo = tipo.lower()
			if tipo == "fibrosa" or tipo == "globular":
				self.__tipo = tipo
			else:
				print("Por favori ingrese si es fibrosa o globular")
		else:
			print("Por favori ingrese si es fibrosa o globular")

	def get_tipo(self):
		return self.__tipo