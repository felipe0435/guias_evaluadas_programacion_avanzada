import sys
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, GObject


class Sexo(GObject.Object):
    """Clase de tipo GObject"""
    __gtype_name__ = 'Sexo'

    def __init__(self, name):
        super().__init__()
        self._name = name

    @GObject.Property
    def name(self):
        return self._name


class MainWindow(Gtk.ApplicationWindow):
	"""ventana principal"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.set_default_size(1920, 1080)#se establece una tamaño predeterminado para la ventana
		self.set_title("Guía evaluada")#Se le da un título a la ventana

		self.search_text = ""

		self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=12)#Se crea una caja para agregar widgets
		self.set_child(self.box)

		sexos = ["Hombre", "Mujer"]

		self.opciones = Gio.ListStore(item_type=Sexo)#Lista que contendrá los GObject
		self.filtrado_opciones = Gtk.FilterListModel(model=self.opciones)
		self.filtro_opciones = Gtk.CustomFilter.new(self._do_filter_view, self.filtrado_opciones)
		self.filtrado_opciones.set_filter(self.filtro_opciones)#filtros son aplicados al ListStore

		for i in sexos:#Se agregan las opciones al Gtk.ListStore
			self.opciones.append(Sexo(name=i))

		factory_sexo = Gtk.SignalListItemFactory()
		factory_sexo.connect("setup", self._on_factory_sexo_setup)
		factory_sexo.connect("bind", self._on_factory_sexo_bind)

		self.dd = Gtk.DropDown(model=self.filtrado_opciones, factory=factory_sexo)#Se crea el dropdown
		self.dd.set_enable_search(True)#Se abilita en el buscador en el dropdown
		self.dd.connect("notify::selected-item", self._on_notify_selection)#se conencta el dropdown con la función _on_notify_selection
		self.box.append(self.dd)

		search_entry = self._get_search_widget(self.dd)#Se extrae el buscador del dropdown
		search_entry.connect('search-changed', self._on_search_widget_changed)#se conecta el buscador con la función _on_search_widget_changed

		self.button1 = Gtk.Button.new_with_label("Print actived text")#botón para imrpimir el texto activo en el dropdown
		self.button1.connect("clicked", self._on_button_pressed, self.dd)#Se conecta el botón con la función _on_button_pressed
		self.box.append(self.button1)


	def _on_notify_selection(self, notify, data):
		"""Función para imprimir lo seleccionado en el dropdown"""
		item = self.dd.get_selected_item()
		if isinstance(item, Sexo):
			print(item.name)
		else:
			print(self.search_text)


	def _get_search_widget(self, dropdown):
		"""Función para encontar el search widget dentro del dropdown"""
		popover = dropdown.get_last_child()
		box = popover.get_child()
		box2 = box.get_first_child()
		search_entry = box2.get_first_child() # Gtk.SearchEntry
		return search_entry


	def _on_search_widget_changed(self, search_entry):
		"""Función para comparar el texto del search widget con las opciones en el dropdown"""
		self.search_text = search_entry.get_text()
		self.filtro_opciones.changed(Gtk.FilterChange.DIFFERENT)


	def _on_button_pressed(self, button, drop_down_text):
		"""Función para imprimir el texto seleccionado en el dropdown cuando se presione el botón vinculado"""
		sexo = drop_down_text.get_selected_item()
		print(sexo.name)


	def _on_factory_sexo_setup(self, factory, list_item):
		"""Inicia la creación del factory para el dropdown"""
		box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
		label = Gtk.Label()
		box.append(label)
		list_item.set_child(box)


	def _on_factory_sexo_bind(self, factory, list_item):
		"""Finaliza la creación del factory"""
		box = list_item.get_child()
		label = box.get_first_child()
		sexo = list_item.get_item()
		label.set_text(sexo.name)


	def _do_filter_view(self, item, list_model):
		"""Se crea el filtro para el Gtk.CustomFilter"""
		return self.search_text.upper() in item.name.upper()


class Myapp(Gtk.Application):
	"""Aplicación"""
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.connect('activate', self.on_activate)


	def on_activate(self, app):
		self.win = MainWindow(application=app)
		self.win.present()


def main():
	app = Myapp(application_id="com.example.GtkApplication")
	app.run(sys.argv)


if __name__ == '__main__':
	main()
