import sys
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, GObject


class Dialog(Gtk.MessageDialog):
	"""Clase MessageDialog"""
	def __init__(self, parent):
		super().__init__(title="Su archivo a sido guardado exitosamente",
		   				transient_for=parent)
		self.set_markup("Presione OK para cerrar esta ventana")
		self.add_button("_OK", Gtk.ResponseType.OK)