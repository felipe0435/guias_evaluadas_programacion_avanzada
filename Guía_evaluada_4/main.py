import sys#Importaciones necesarias para usar Gtk y crear la aplicación
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, GObject
from dialog import Dialog#Se importa la clase Dialog para utilizar más tarde


class MainWindow(Gtk.ApplicationWindow):
	"""ventana principal"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.set_default_size(1920, 1080)#se establece una tamaño predeterminado para la ventana
		self.set_title("Guía evaluada")#Se le da un título a la ventana

		self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)#Se crea una caja para agregar widgets
		self.set_child(self.box)

		self.label = Gtk.Label()#Se agrega un texto para guiar al usuario
		self.box.append(self.label)
		self.label.set_size_request(300, 30)
		self.label.set_text("Bienvenido, ingrese su texto")

		self.entry = Gtk.Entry()#Se crea un espacio para que el usuario ingrese texto
		self.box.append(self.entry)

		self.save_button = Gtk.Button(label="Guardar")#Se crea un botón para guardar el texto escrito
		self.save_button.connect('clicked', self.save)
		self.box.append(self.save_button)

		self.native = self.text_save()
		self.native.connect('response', self.on_file_save_response)


	def save(self, button):
		"""Se muestra la ventana para guardar archivos"""
		self.native.show()


	def text_save(self):
		"""Se crea la ventana para guardar archivos"""
		return Gtk.FileChooserNative(title="Guardar archivo",
			       					action=Gtk.FileChooserAction.SAVE,
									accept_label="_Guardar",
									cancel_label="_Cancelar"
									)


	def on_file_save_response(self, native, response):
		"""Según la respuesta, el archivo se guarda o no"""
		if response == Gtk.ResponseType.ACCEPT:
			_path = native.get_file().get_path()
			with open(_path, "w") as _file:
				_file.write(f"{self.entry.get_text()}\n")
			self.open_dialog()


	def open_dialog(self):
		"""Se crea y muestra la ventana que da aviso que el archivo se guardó de manera exitosa"""
		dialog = Dialog(parent=self.get_root())
		dialog.connect("response", self.on_dialog_response)
		dialog.set_visible(True)


	def on_dialog_response(self, dialog, response):
		"""Confirma que el usuario haya presionado el bótón para cerrar la ventana"""
		if response == Gtk.ResponseType.OK:
			dialog.close()


class Myapp(Gtk.Application):
	"""Aplicación"""
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.connect('activate', self.on_activate)


	def on_activate(self, app):
		self.win = MainWindow(application=app)
		self.win.present()


def main():
	app = Myapp(application_id="com.example.GtkApplication")
	app.run(sys.argv)


if __name__ == '__main__':
	main()