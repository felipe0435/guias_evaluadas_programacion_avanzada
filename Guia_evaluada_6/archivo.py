import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, GObject


class Archivo(GObject.Object):
    """Clase de tipo GObject"""
    __gtype_name__ = 'Archivo'

    def __init__(self, name):
        super().__init__()
        self._name = name

    @GObject.Property
    def name(self):
        return self._name