import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, GObject


class Dialog(Gtk.AboutDialog):
	"""Clase MessageDialog"""
	def __init__(self, parent):
		super().__init__()
		self.set_authors(["Felipe Méndez"])
		self.set_program_name("Chemicals visualizer")