import sys
import gi
import pathlib
gi.require_version('Gtk', '4.0')
from os import remove
from gi.repository import Gtk, Gio, GObject, GLib, GdkPixbuf
from dialog import Dialog
from archivo import Archivo
from rdkit import Chem
from rdkit.Chem import Draw


class MainWindow(Gtk.ApplicationWindow):
	"""ventana principal"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.set_default_size(1920, 1080)#se establece una tamaño predeterminado para la ventana

		self.search_text = ""
		self.archivos_mol = []
		self.path = None

		self.header = Gtk.HeaderBar.new()
		self.header_label = Gtk.Label.new()
		self.header_label.set_text("Chemicals visualizer")
		self.header.set_title_widget(self.header_label)

		self.header_menu = Gio.Menu.new()
		self.header_menu.append('Informacion', 'app.preferences')
		self.set_titlebar(titlebar=self.header)

		self.menu_button = Gtk.MenuButton.new()
		self.menu_button.set_icon_name(icon_name='open-menu-symbolic')
		self.menu_button.set_menu_model(self.header_menu)
		self.header.pack_end(self.menu_button)

		self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=12)#Se crea una caja para agregar widgets
		self.set_child(self.box)

		self.open_button = Gtk.Button(label="Open")
		self.open_button.connect('clicked', self.open)

		self.open_native = self.text_open()
		self.open_native.connect('response', self.on_file_open_response)

		self.opciones = Gio.ListStore(item_type=Archivo)#Lista que contendrá los GObject
		self.filtrado_opciones = Gtk.FilterListModel(model=self.opciones)
		self.filtro_opciones = Gtk.CustomFilter.new(self._do_filter_view, self.filtrado_opciones)
		self.filtrado_opciones.set_filter(self.filtro_opciones)#filtros son aplicados al ListStore

		factory_sexo = Gtk.SignalListItemFactory()
		factory_sexo.connect("setup", self._on_factory_sexo_setup)
		factory_sexo.connect("bind", self._on_factory_sexo_bind)

		self.dd = Gtk.DropDown(model=self.filtrado_opciones, factory=factory_sexo)#Se crea el dropdown
		self.dd.set_enable_search(True)#Se abilita en el buscador en el dropdown
		self.dd.connect("notify::selected-item", self._on_notify_selection)#se conencta el dropdown con la función _on_notify_selection
		self.box.append(self.dd)

		search_entry = self._get_search_widget(self.dd)#Se extrae el buscador del dropdown
		search_entry.connect('search-changed', self._on_search_widget_changed)#se conecta el buscador con la función _on_search_widget_changed

		self.image = Gtk.Image.new()
		self.image.set_pixel_size(300)
		self.box.append(self.image)

		self.box.append(self.open_button)


	def _on_notify_selection(self, notify, data):
		item = self.dd.get_selected_item()
		item = item.name
		path = f"{self.path}/{item}.mol"
		m = Chem.MolFromMolFile(path)
		img = Draw.MolToImage(m)
		buffer = GLib.Bytes.new(img.tobytes())
		gdata = GdkPixbuf.Pixbuf.new_from_bytes(buffer,
												GdkPixbuf.Colorspace.RGB,
												False, 8, img.width, img.height,
												len(img.getbands())*img.width)
		self.image.set_from_pixbuf(gdata)


	def _get_search_widget(self, dropdown):
		"""Función para encontar el search widget dentro del dropdown"""
		popover = dropdown.get_last_child()
		box = popover.get_child()
		box2 = box.get_first_child()
		search_entry = box2.get_first_child() # Gtk.SearchEntry
		return search_entry


	def _on_search_widget_changed(self, search_entry):
		"""Función para comparar el texto del search widget con las opciones en el dropdown"""
		self.search_text = search_entry.get_text()
		self.filtro_opciones.changed(Gtk.FilterChange.DIFFERENT)


	def _on_button_pressed(self, button, drop_down_text):
		"""Función para imprimir el texto seleccionado en el dropdown cuando se presione el botón vinculado"""
		sexo = drop_down_text.get_selected_item()
		print(sexo.name)


	def _on_factory_sexo_setup(self, factory, list_item):
		"""Inicia la creación del factory para el dropdown"""
		box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
		label = Gtk.Label()
		box.append(label)
		list_item.set_child(box)


	def _on_factory_sexo_bind(self, factory, list_item):
		"""Finaliza la creación del factory"""
		box = list_item.get_child()
		label = box.get_first_child()
		sexo = list_item.get_item()
		label.set_text(sexo.name)


	def _do_filter_view(self, item, list_model):
		"""Se crea el filtro para el Gtk.CustomFilter"""
		return self.search_text.upper() in item.name.upper()


	def open(self, button):
		"""Se muestra la ventana para abrir un archivo"""
		self.open_native.show()


	def text_open(self):
		"""Se crea la ventana para abrir archivos"""
		return Gtk.FileChooserNative(title="Seleccionar archivo",
			       					action=Gtk.FileChooserAction.SELECT_FOLDER,
									accept_label="_Abrir",
									cancel_label="_Cancelar"
									)


	def on_file_open_response(self, native, response):
		"""Según la respuesta el archivo se abre o no"""
		if response == Gtk.ResponseType.ACCEPT:
			self.path = native.get_file().get_path()
			directorio = self.path
			directorio = pathlib.Path(str(directorio))
			self.archivos_mol = [fichero.name for fichero in directorio.iterdir() if
			directorio.glob("*.mol")]
			for i in self.archivos_mol:#Se agregan las opciones al ListStore
				i = i.split(".mol")
				i = i[0]
				self.opciones.append(Archivo(name=i))


class Myapp(Gtk.Application):
	"""Aplicación"""
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.connect('activate', self.on_activate)

		self.create_action('preferences', self.on_preferences_action)


	def on_activate(self, app):
		self.win = MainWindow(application=app)
		self.win.present()


	def on_preferences_action(self, action, param):
		dialog = Dialog(parent=self)
		dialog.set_visible(True)


	def create_action(self, name, callback, shortcuts=None):
		action = Gio.SimpleAction.new(name, None)
		action.connect('activate', callback)
		self.add_action(action)
		if shortcuts:
			self.set_accels_for_action(f'app.{name}', shortcuts)


def main():
	app = Myapp(application_id="com.example.GtkApplication")
	app.run(sys.argv)


if __name__ == '__main__':
	main()