class Vacuna:
	def __init__(self):
		self.__nombre = None
		self.__lab = None
		self.__efectos = []

	def set_nombre(self, nombre):
		if isinstance(nombre, str):
			self.__nombre = nombre
		else:
			print("Por favor ingrese un nombre válido.")

	def get_nombre(self):
		return self.__nombre

	def set_lab(self, nombre):
		if isinstance(nombre, str):
			self.__lab = nombre
		else:
			print("Por favor ingrese un nombre válido.")
	
	def get_lab(self):
		return self.__lab
	
	def set_efecto_secundario(self, efectos):
		if isinstance(efectos, str):
			for efecto in efectos.split(','):
				self.__efectos.append(efecto)
		else:
			print("Por favor ingrese un nombre válido.")

	def show_efecto_secundario(self):
		print("\nLos efectos secundarios de", self.__nombre, "son:")
		for efecto in self.__efectos:
			print(efecto)	
