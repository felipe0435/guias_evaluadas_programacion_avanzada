from vacuna import Vacuna

def nueva_vacuna(nom, lab, efectos):
	nuevo = Vacuna()
	nuevo.set_nombre(nom)
	nuevo.set_lab(lab)
	efectos = efectos.replace(', ', ',')
	nuevo.set_efecto_secundario(efectos)
	return nuevo

def main():
	efectos = "dolor en el lugar de la inyección, fatiga, dolor de cabeza, escalofríos y fiebre"
	pfizer = nueva_vacuna("Pfizer", "Pfizer", efectos)

	moderna = nueva_vacuna("Moderna", "Moderna", efectos)

	efectos = "dolor en el lugar de la inyección, fatiga, dolor de cabeza y dolor muscular"
	jj = nueva_vacuna("Johnson & Johnson/Jassen", "Jassen pharmaceuticals", efectos)

	pfizer.show_efecto_secundario()
	moderna.show_efecto_secundario()
	jj.show_efecto_secundario()
	

if __name__ == "__main__":
	main()