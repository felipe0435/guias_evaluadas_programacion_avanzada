from planeta import Planeta

class Sistema:
	def __init__(self) -> None:
		self.__planetas = {}
	
	def set_planeta(self, planetas):
		if isinstance(planetas, list):
			for planeta in planetas:
				if isinstance(planeta, Planeta):
					self.__planetas[planeta.get_id()] = planeta

	def get_planetas(self):
		return self.__planetas

	def show_masa_planeta(self, id):
		planeta = self.__planetas[id]
		print("La masa de", planeta.get_nombre(), "es:")
		print(planeta.get_masa(), "u.a.m")

	def show_densidad_planeta(self, id):
		planeta = self.__planetas[id]
		print("La densidad de", planeta.get_nombre(), "es:")
		print(planeta.get_densidad(), "kg/m^3")

	def show_diametro_planeta(self, id):
		planeta = self.__planetas[id]
		print("El diámetro de", planeta.get_nombre(), "es:")
		print(planeta.get_diametro(), "km")
	
	def show_distancia_planeta(self, id):
		planeta = self.__planetas[id]
		print("La distancia con el sol de", planeta.get_nombre(), "es:")
		print(planeta.get_distancia(), "ua")

	def show_planetas(self):
		print("Los planetas en el sistema solar son:")
		for planeta in self.__planetas:
			print(self.__planetas[planeta].get_nombre())