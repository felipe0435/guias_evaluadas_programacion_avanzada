from sistema import Sistema
from planeta import Planeta

def crear_planeta(nom, id, masa, dia, den, dis):
	nuevo = Planeta()
	nuevo.set_nombre(nom)
	nuevo.set_id(id)
	nuevo.set_masa(masa)
	nuevo.set_diametro(dia)
	nuevo.set_densidad(den)
	nuevo.set_distancia(dis)
	return nuevo

def lista_planetas():
	planetas = []
	mercurio = crear_planeta("Mercurio", 1, 0.0553, 4880.0, 5427.0, 0.39)
	planetas.append(mercurio)
	venus = crear_planeta("Venus", 2, 0.815, 12104.0, 5243.0, 0.72)
	planetas.append(venus)
	tierra = crear_planeta("Tierra", 3, 1.0, 12742.0, 5514.0, 1.0)
	planetas.append(tierra)
	marte = crear_planeta("Marte", 4, 0.107, 6779.0, 3933.0, 1.52)
	planetas.append(marte)
	jupiter = crear_planeta("Júpiter", 5, 317.8, 139822.0, 1326.0, 5.20)
	planetas.append(jupiter)
	saturno = crear_planeta("Saturno", 6, 95.16, 116460.0, 687.0, 9.58)
	planetas.append(saturno)
	urano = crear_planeta("Urano", 7, 14.54, 50724.0, 1270.0, 19.18)
	planetas.append(urano)
	neptuno = crear_planeta("Neptuno", 8, 17.15, 49244.0, 1638.0, 30.07)
	planetas.append(neptuno)
	return planetas
	
def mostrar_planetas(sistema):
	i = 1
	for planeta in sistema.get_planetas():
		sistema.show_masa_planeta(i)
		sistema.show_densidad_planeta(i)
		sistema.show_diametro_planeta(i)
		sistema.show_distancia_planeta(i)
		i += 1

def main():
	planetas = lista_planetas()
	sistema = Sistema()
	sistema.set_planeta(planetas)
	mostrar_planetas(sistema)


if __name__ == "__main__":
	main()