class Planeta:
	def __init__(self) -> None:
		self.__nombre = None
		self.__id = None
		self.__masa = None
		self.__densidad = None
		self.__diametro = None
		self.__distancia = None

	def set_nombre(self, nombre):
		if isinstance(nombre, str):
			self.__nombre = nombre
		else:
			print("Por favor ingrese un nombre válido.")

	def get_nombre(self):
		return self.__nombre

	def set_id(self, id):
		if isinstance(id, int):
			self.__id = id
		else:
			print("por favor ingrese un número válido.")

	def get_id(self):
		return self.__id
	
	def set_masa(self, masa):
		if isinstance(masa, float):
			self.__masa = masa
		else:
			print("Por favor ingrese un valor válido.")

	def get_masa(self):
		return self.__masa
	
	def set_densidad(self, dens):
		if isinstance(dens, float):
			self.__densidad = dens
		else:
			print("Por favor ingrese un valor válido.")

	def get_densidad(self):
		return self.__densidad
	
	def set_diametro(self, diam):
		if isinstance(diam, float):
			self.__diametro = diam
		else:
			print("Por favor ingrese un valor válido.")

	def get_diametro(self):
		return self.__diametro
	
	def set_distancia(self, dis):
		if isinstance(dis, float):
			self.__distancia = dis
		else:
			print("Por favor ingrese un valor válido.")

	def get_distancia(self):
		return self.__distancia