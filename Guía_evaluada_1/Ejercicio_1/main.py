from perro import Perro
from time import sleep

def crear_perros():
	perros = {}
	canela = Perro()
	canela.set_nombre("Canela")
	ambar = Perro()
	ambar.set_nombre("Ámbar")
	pe = [canela, ambar]

	for perro in pe:
		perros[perro.get_nombre()] = perro

	return perros	

def eleccion(perros):
	nombre = input("Ingrese el nombre del perro:\n")
	if  not (nombre in perros):
		nuevo = Perro()
		nuevo.set_nombre(nombre)
		perros[nombre] = nuevo
	return nombre, perros

def acciones(perros, nom):
	while True:
		print("¿Qué acción desea realizar?\n1: pasear.\n2: tomar agua."
"\n3: dejar de pasear.\n4: continuar.\n5: Seleccionar otro perro.\n6: Detener el programa.")
		pregunta = input()
		if pregunta == "1":
			perros[nom].pasear()
		elif pregunta == "2":
			perros[nom].tomar_agua()
		elif pregunta == "3":
			perros[nom].para()
		elif pregunta == "4":
			if perros[nom].get_pasear():
				print(perros[nom].get_nombre(), "sigue paseando.")
			else:
				print(perros[nom].get_nombre(), "sigue detenido/a.")
			pass
		elif pregunta == "5":
			return False
		elif pregunta == "6":
			return True
		else:
			print("Por favor ingrese una de las opciones en pantalla.")
		perros[nom].set_hora_toma_agua(1)
		sleep(1)

def main():
	perros = crear_perros()
	while True:
		nombre, perros = eleccion(perros)
		if acciones(perros, nombre):
			break

	

if __name__ == "__main__":
	main()