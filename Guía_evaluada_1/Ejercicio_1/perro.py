class Perro:
	def __init__(self):
		self.__nombre = None
		self.__pasear = False
		self.__agua = 0

	def get_nombre(self):
		return self.__nombre
	
	def set_nombre(self, nombre):
		if isinstance(nombre, str):
			self.__nombre = nombre
		else:
			print("Por favor ingrese texto para el nombre.")

	def set_hora_toma_agua(self, h):
		if isinstance(h, int):
			self.__agua = self.__agua + h
		
	
	def get_toma_agua(self):
		return self.__agua
	
	def tomar_agua(self):
		if not self.__pasear:
			self.__agua = 0
			print(self.__nombre, "tomó agua.")
		else:
			print(self.__nombre, "no puede tomar agua, está paseando.")

	def para(self):
		if self.__pasear:
			self.__pasear = False
			print(self.__nombre, "dejó de pasear.")
		else:
			print(self.__nombre, "ya está detenido/a.")
	
	def pasear(self):
		print(self.__agua)
		if self.__pasear:
			print(self.__nombre, "ya está paseando.")
		elif self.__agua < 4:
			self.__pasear = True
			print(self.__nombre, "puede pasear.")
		else:
			print(self.__nombre, "no puede pasear, necesita agua.")

	def get_pasear(self):
		if self.__pasear :
			return True
		else:
			return False