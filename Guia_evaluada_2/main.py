from random import randint
from secuenciador import Secuenciador
from secuencia import Secuencia

def letras(secuencia):
	for i in range (0, 100):
		random = randint(1,4)
		if random == 1:
			secuencia.set_secuencia("A")
		elif random == 2:
			secuencia.set_secuencia("C")
		elif random == 3:
			secuencia.set_secuencia("G")
		elif random == 4:
			secuencia.set_secuencia("T")

def main():
	adn = Secuenciador()
	letras(adn)
	adn.set_inverso()
	adn.calcular_longitud()
	adn.contar_nucleotidos()

	print(adn.get_secuencia().get_cadena())
	print(adn.get_inverso().get_cadena())
	print("Esta cadena tiene:",adn.get_secuencia().get_longitud(), "nucleótidos.")
	adn.mostrar_num_nucleotidos()

	adn.encontrar_patron("AT")
	adn.calcular_peso_molecular()

if __name__ == "__main__":
	main()