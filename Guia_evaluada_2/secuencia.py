class Secuencia:
	def __init__(self) -> None:
		self.__cadena = None
		self.__longitud = 0

	def set_cadena(self, sec):
		if isinstance(sec, str):
			if self.__cadena != None:
				self.__cadena = "{}{}".format(self.__cadena, sec)
			else:
				self.__cadena = sec
		
	def get_cadena(self):
		return self.__cadena

	def set_longitud(self, long):
		if isinstance(long, int):
			self.__longitud = long

	def get_longitud(self):
		return self.__longitud