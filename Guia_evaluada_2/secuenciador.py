from secuencia import Secuencia

class Secuenciador:
	def __init__(self) -> None:
		self.__secuencia = Secuencia()
		self.__inverso = Secuencia()
		self.__nucleotidos = {"A": 0, "C": 0, "G": 0, "T": 0}
		self.__peso_molecular = 0

	def set_secuencia(self, nuc):
		if isinstance(nuc, str):
			if nuc == "A" or nuc == "C" or nuc == "G" or nuc == "T":
				self.__secuencia.set_cadena(nuc)

	def set_inverso(self):
		dic = {"A": "T", "G": "C", "C": "G", "T": "A"}
		cadena = self.__secuencia.get_cadena()[::-1]
		for letra in cadena:
			self.__inverso.set_cadena(dic[letra])

	def calcular_longitud(self):
		cont = 0
		for letra in self.__secuencia.get_cadena()[::1]:
			cont += 1
		self.__secuencia.set_longitud(cont)
		self.__inverso.set_longitud(cont)

	def get_secuencia(self):
		return self.__secuencia
	
	def get_inverso(self):
		return self.__inverso
	
	def contar_nucleotidos(self):
		cadena = self.__secuencia.get_cadena()
		for letra in cadena[::1]:
			self.__nucleotidos[letra] += 1

	def mostrar_num_nucleotidos(self):
		dic = self.__nucleotidos
		print("Esta secuencia tiene:")
		print(dic["A"], "Nucleótidos A")
		print(dic["C"], "Nucleótidos C")
		print(dic["G"], "Nucleótidos G")
		print(dic["T"], "Nucleótidos T")
		
	def get_nucleotidos(self):
		return self.__nucleotidos
	
	def encontrar_patron(self, patron):
		cadena = self.__secuencia.get_cadena()
		nucle = []
		posiciones = []
		long = 0
		cont = 0
		temp = ""
		for letra in patron:
			long += 1
			
		
		for letra in cadena[::1]:
			nucle.append(letra)
		
		for elem in nucle:
			for i in range (0, long):
				if i + cont < len(nucle):
					temp = "{}{}".format(temp, nucle[i + cont])

			if temp == patron:
				posiciones.append(cont)
				temp = ""
			else:
				temp = ""
			cont += 1
				
		
		if posiciones != []:
			print ("Este patrón existe en las posicion/es:")
			for elem in posiciones:
				print(elem + 1)

	def calcular_peso_molecular(self):
		valores = {"A": 313.21, "T": 304.2, "C": 289.18, "G": 329.21}
		cadena = self.__secuencia.get_cadena()
		total = 0
		for nuc in cadena[::1]:
			total += valores[nuc]
		self.__peso_molecular = total
		print("El peso molecular es:\n{}".format(total))

	def get_peso_molecular(self):
		return self.__peso_molecular
				
